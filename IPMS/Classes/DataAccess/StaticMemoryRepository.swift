//
//  StaticMemoryRepository.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 17-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit

class StaticMemoryRepository: StorageRepository {
    
    static let sharedInstance = StaticMemoryRepository()
    
    private var incidents: [IncidentEntity] = []
    private var patients: [PatientEntity] = []
    
    private init() {
        
        var incident = IncidentEntity.init(name: "Incident 1", startDate: Date.init(timeIntervalSinceNow: -60 * 60 * 24))
        self.incidents.append(incident)
        
        incident = IncidentEntity.init(name: "Incident 2", startDate: Date.init(timeIntervalSinceNow: -60 * 60 * 24 * 3))
        self.incidents.append(incident)
    }
    
    func incidentList() -> [IncidentEntity] {
        return incidents
    }
    
    func addIncident(_ incident: IncidentEntity) {
        self.incidents.append(incident)
    }
    
    func patientList() -> [PatientEntity] {
        return patients
    }
    
    func addPatient(_ patient: PatientEntity) {
        self.patients.append(patient)
    }
}
