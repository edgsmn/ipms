//
//  StorageFactory.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 17-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit


protocol StorageRepository {
    func incidentList() -> [IncidentEntity]
    func addIncident(_ incident: IncidentEntity)
    
    func patientList() -> [PatientEntity]
    func addPatient(_ patient: PatientEntity)
}

/// Created in case database is used later on for easy implementation switch
class RepositoryFactory: NSObject {
    static func repository() -> StorageRepository {
        return StaticMemoryRepository.sharedInstance
    }

}
