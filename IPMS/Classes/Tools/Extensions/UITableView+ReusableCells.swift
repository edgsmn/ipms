//
//  UITableView+ReusableCells.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 17-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func register(cellClass tableViewCellClass: AnyClass) {
        register(UINib(nibName: String(describing: tableViewCellClass), bundle: nil), forCellReuseIdentifier: String(describing: tableViewCellClass))
    }
    
    func dequeueReusableCell(withCell tableViewCellClass: AnyClass) -> UITableViewCell? {
        return dequeueReusableCell(withIdentifier: String(describing: tableViewCellClass))
    }
}

