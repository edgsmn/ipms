//
//  Triage.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 17-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit

enum TriageCategory: Int, CaseIterable {
    case black = 0
    case red
    case yellow
    case green
    
    
    func color() -> UIColor {
        switch self {
        case .black:
            return UIColor.black
            
        case .red:
            return UIColor.red
            
        case .yellow:
            return UIColor.yellow
            
        case .green:
            return UIColor.green
        }
    }
    
    func name() -> String {
        switch self {
        case .black:
            return "Black"
            
        case .red:
            return "Red"
            
        case .yellow:
            return "Yellow"
            
        case .green:
            return "Green"
        }
    }
}
