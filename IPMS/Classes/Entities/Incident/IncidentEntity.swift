//
//  IncidentEntity.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 17-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit

class IncidentEntity: NSObject {
    
    // MARK: - Declarations
    let name: String
    let startDate: Date
    
    init(name: String, startDate: Date) {
        self.name = name
        self.startDate = startDate
    }
    
    func dateString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        return dateFormatter.string(from: startDate)
    }
}
