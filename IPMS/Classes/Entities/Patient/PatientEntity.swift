
//
//  PatientEntity.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 17-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit

class PatientEntity {
    
    let incidentName: String
    let firstName: String
    let lastName: String
    let triageCategory: TriageCategory
    
    init(withIncidentName incidentName: String, firstName: String, lastName: String, triageCategory: TriageCategory) {
        
        self.incidentName = incidentName
        self.firstName = firstName
        self.lastName = lastName
        self.triageCategory = triageCategory
    }
}
