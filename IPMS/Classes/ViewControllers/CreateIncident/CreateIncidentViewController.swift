//
//  CreateIncidentViewController.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 17-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit

protocol CreateIncidentViewControllerDelegate: class {
    func createIncidentViewController(_ viewController: CreateIncidentViewController, didCreateIncident incident: IncidentEntity)
    func createIncidentViewControllerDidCancel(_ viewController: CreateIncidentViewController)
}

class CreateIncidentViewController: BaseViewController, UITextFieldDelegate {
    
    // MARK: - Declarations
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var startDateTextField: UITextField!
    
    let dateFormatter = DateFormatter()
    
    
    weak var delegate: CreateIncidentViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Register Incident"

        navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "navbar-logout"), style: .plain, target: self, action: #selector(logout))

        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        startDateTextField.inputView = datePickerInputView()
        
        let gestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        gestureRecognizer.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(gestureRecognizer)
    }
    
    // MARK: - UI actions
    
    @objc func cancel() {
        delegate?.createIncidentViewControllerDidCancel(self)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        if let datePicker = startDateTextField.inputView as? UIDatePicker {
            
            if datePicker.date > Date() {
                showAlert(with: "Error", message: "Incidents cannot created in the future")
                return
            }
            
            let incident = IncidentEntity(name: nameTextField.text ?? "No name", startDate: datePicker.date)
            delegate?.createIncidentViewController(self, didCreateIncident: incident)
        } else {
            showAlert(with: "Error", message: "Incident creation failed")
        }
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    
    // MARK: - UIDatePicker
    
    func datePickerInputView() -> UIView {
        
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 40, width: 0, height: 0))
        datePicker.datePickerMode = .dateAndTime
        datePicker.locale = Locale.current
        datePicker.setDate(Date(), animated: true)
//        datePicker.maximumDate = Date()
        
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
        datePickerValueChanged(sender: datePicker)
        
        return datePicker
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        startDateTextField.text = dateFormatter.string(from: sender.date)
    }
}
