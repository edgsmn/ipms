//
//  IncidentsDataModel.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 17-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit

enum IncidentSortingOption: String, CaseIterable {
    case nameASC = "Name ascending"
    case nameDESC = "Name descending"
    case dateASC = "Date ascending"
    case dateDESC = "Date descending"
}

protocol IncidentListDataModelDelegate: class {
    func incidentsDataModelDelgateDidUpdateData(_ dataModel: IncidentListDataModel)
}

class IncidentListDataModel {
    
    // MARK: - Declarations
    
    weak var delegate: IncidentListDataModelDelegate?
    
    private var defaultList: [IncidentEntity] = []
    public private(set) var listToDisplay: [IncidentEntity] = []
    
    private var sortingOpton: IncidentSortingOption?
    
    // MARK: - Methods
    
    init() {
        loadIncidents()
    }
    
    func sort(by option:IncidentSortingOption) {
        
        switch option {
        case .nameASC:
            listToDisplay = defaultList.sorted(by: { $0.name < $1.name })
            
        case .nameDESC:
            listToDisplay = defaultList.sorted(by: { $0.name > $1.name })

        case .dateASC:
            listToDisplay = defaultList.sorted(by: { $0.startDate < $1.startDate })
            
        case .dateDESC:
            listToDisplay = defaultList.sorted(by: { $0.startDate > $1.startDate })
        }
        
        sortingOpton = option
        delegate?.incidentsDataModelDelgateDidUpdateData(self)
    }
    
    func filterList(for searchString: String) {
        
        if searchString == "" {
            listToDisplay = defaultList
        } else {
            listToDisplay = defaultList.filter({ (incident: IncidentEntity) -> Bool in
                return incident.name.localizedCaseInsensitiveContains(searchString)
                }
            )
        }
        
        if let option = sortingOpton {
            sort(by: option)
        }
        
        delegate?.incidentsDataModelDelgateDidUpdateData(self)
    }
    
    func addIncident(_ incident: IncidentEntity) {
        RepositoryFactory.repository().addIncident(incident)
        loadIncidents()
    }
    
    private func loadIncidents() {
        defaultList = RepositoryFactory.repository().incidentList()
        listToDisplay = defaultList
        sortingOpton = nil
        delegate?.incidentsDataModelDelgateDidUpdateData(self)
    }
}
