//
//  IncidentListViewController.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 16-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit

class IncidentListViewController: BaseListViewController, IncidentListDataModelDelegate, CreateIncidentViewControllerDelegate {

    // MARK: - Declarations
    
    let dataModel = IncidentListDataModel()
    
    // MARK: - Methods
    
    init() {
        super.init(nibName: String(describing: BaseListViewController.self), bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Incidents"
        dataModel.delegate = self
                
        let addItem = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(addIncident))
        let logoutItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "navbar-logout"), style: .plain, target: self, action: #selector(logout))
        let sortItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "navbar-sort"), style: .plain, target: self, action: #selector(sort))
        
        navigationItem.rightBarButtonItems = [addItem, sortItem, logoutItem]
        
    }
    
    // MARK: - BaseListViewController
    
    override func listToDisplay() -> [Any] {
        return dataModel.listToDisplay
    }
    
    override func customCell(for tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: "IncidentsCell")
        
        if let incident = listToDisplay()[indexPath.row] as? IncidentEntity {
            
            cell.textLabel?.text = incident.name
            cell.detailTextLabel?.text = incident.dateString()
        }
        
        return cell
    }
    
    override func filterList(with text: String) {
        dataModel.filterList(for: text)
    }
    
    // MARK: - IncidentListDataModelDelegate
    
    func incidentsDataModelDelgateDidUpdateData(_ dataModel: IncidentListDataModel) {
        tableView.reloadData()
    }
    
    // MARK: - CreateIncidentViewControllerDelegate
    
    func createIncidentViewController(_ viewController: CreateIncidentViewController, didCreateIncident incident: IncidentEntity) {
        dataModel.addIncident(incident)
        navigationController?.popViewController(animated: true)
    }
    
    func createIncidentViewControllerDidCancel(_ viewController: CreateIncidentViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - UI actions
    
    @objc func addIncident() {
        let createIncidentViewController = CreateIncidentViewController()
        createIncidentViewController.delegate = self
        navigationController?.pushViewController(createIncidentViewController, animated: true)
    }
    
    @objc func sort() {
        
        let alertController = UIAlertController.init(title: "Sort by", message: nil, preferredStyle: .alert)
        
        IncidentSortingOption.allCases.forEach {
            let option: IncidentSortingOption = $0
            let action = UIAlertAction.init(title: option.rawValue, style: .default, handler: { (action) in
                self.dataModel.sort(by: option)
            })
            alertController.addAction(action)
        }
        
        present(alertController, animated: true, completion: nil)
    }
}
