//
//  BaseViewController.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 16-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit
import GoogleSignIn

class BaseViewController: UIViewController {
    
    // MARK: - Declarations
    
    var activityIndicatorBackgroundView: UIView!
    var activityIndicator: UIActivityIndicatorView!
    
    private let backgroundViewAlpha: CGFloat = 0.6
    
    // MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createBackgroundView()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        view.topAnchor.constraint(equalTo: activityIndicatorBackgroundView.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: activityIndicatorBackgroundView.bottomAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: activityIndicatorBackgroundView.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: activityIndicatorBackgroundView.trailingAnchor).isActive = true
        
        activityIndicator.centerXAnchor.constraint(equalTo: activityIndicatorBackgroundView.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: activityIndicatorBackgroundView.centerYAnchor).isActive = true
    }
    
    // MARK: - UIActivityIndicator
    
    func showActivityIndicator() {
        activityIndicator.startAnimating()
        view.bringSubviewToFront(activityIndicatorBackgroundView)
        activityIndicatorBackgroundView.isHidden = false
    }
    
    func hideActivityIndicator() {
        activityIndicatorBackgroundView.isHidden = true
        activityIndicator.stopAnimating()
    }
    
    private func createBackgroundView() {
        let screenSize = UIScreen.main.bounds
        activityIndicatorBackgroundView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
        view.addSubview(activityIndicatorBackgroundView)
        activityIndicatorBackgroundView.backgroundColor = .black
        activityIndicatorBackgroundView.alpha = backgroundViewAlpha
        
        activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicatorBackgroundView.addSubview(activityIndicator)
        
        activityIndicator.center = CGPoint(x: activityIndicatorBackgroundView.frame.width / 2,
                                           y: activityIndicatorBackgroundView.frame.height / 2)
        activityIndicatorBackgroundView.isHidden = true
    }
    
    // MARK: - Logout
    
    @objc func logout() {
        
        let alertController = UIAlertController.init(title: "Are you sure?", message: "This will log you out of the system", preferredStyle: .actionSheet)
        
        let logoutAction = UIAlertAction.init(title: "Log out", style: .destructive) { (action) in
            GIDSignIn.sharedInstance()?.disconnect()
        }
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(logoutAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Helpers
    
    func showAlert(with title: String?, message: String?, closure: (() -> Void)? = nil) {
        
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "OK", style: .default) { (action) in
            closure?()
        }
        
        alertController.addAction(okAction)
        
        present(alertController, animated: true, completion: nil)
    }
}
