//
//  BaseListViewController.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 17-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit

class BaseListViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottomSpacingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startMonitoringKeyboardNotifications()
    }
    
    deinit {
        stopMonitoringKeyboardNotifications()
    }
    
    // MARK: - Base class methods to override
    
    func listToDisplay() -> [Any] {
        assert(false, "\(String(describing: self)) listToDisplay - not overriden")
        return []
    }
    
    func customCell(for tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        assert(false, "\(String(describing: self)) customCell - not overriden")
        return UITableViewCell()
    }
    
    func filterList(with text: String) {
        assert(false, "\(String(describing: self)) filterList - not overriden")
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return customCell(for: tableView, at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listToDisplay().count
    }
    
    // MARK: - UISearchBarDelegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterList(with: searchBar.text ?? "")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.endEditing(true)
    }
    
    // MARK: - Keyboard handling
    
    func startMonitoringKeyboardNotifications() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIWindow.keyboardWillShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIWindow.keyboardWillHideNotification,
                                               object: nil)
    }
    
    func stopMonitoringKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self,
                                                  name: UIWindow.keyboardWillShowNotification,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: UIWindow.keyboardWillHideNotification,
                                                  object: nil)
        
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        
        var keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as AnyObject).cgRectValue.height
        
        if let tabBarController = tabBarController {
            keyboardHeight = keyboardHeight - tabBarController.tabBar.frame.size.height
        }
        
        UIView.animate(withDuration: 0.1) {
            self.tableViewBottomSpacingConstraint.constant = keyboardHeight
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        
        UIView.animate(withDuration: 0.1) {
            self.tableViewBottomSpacingConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}
