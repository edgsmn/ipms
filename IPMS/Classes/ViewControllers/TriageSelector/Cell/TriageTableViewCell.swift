//
//  TriageTableViewCell.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 17-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit

class TriageTableViewCell: UITableViewCell {


    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var colorView: UIView!
    
    func populate(with triage: TriageCategory?) {
        
        if let triage = triage {
            nameLabel.text = triage.name()
            colorView.backgroundColor = triage.color()
        } else {
            nameLabel.text = "None"
            colorView.backgroundColor = UIColor.clear
        }

    }
    
    static func heightForCell() -> CGFloat {
        return 60
    }
}
