//
//  TriageSelectorViewController.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 17-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit

protocol TriageSelectorViewControllerDelegate: class {
    func triageSelectorViewController(_ viewController: TriageSelectorViewController, didSelectTriage triage: TriageCategory?)
}

class TriageSelectorViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var holderViewHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: TriageSelectorViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(cellClass: TriageTableViewCell.self)
        holderViewHeightConstraint.constant = CGFloat(TriageCategory.allCases.count + 1) * TriageTableViewCell.heightForCell()
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TriageCategory.allCases.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withCell: TriageTableViewCell.self) as? TriageTableViewCell else {
            print("WARNING! TableView failed to dequeue reusable cell with identifier ",String(describing: TriageTableViewCell.self))
            return UITableViewCell()
        }
        
        
        cell.populate(with: TriageCategory.init(rawValue: indexPath.row))
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.triageSelectorViewController(self, didSelectTriage: TriageCategory.init(rawValue: indexPath.row))
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return TriageTableViewCell.heightForCell()
    }
}
