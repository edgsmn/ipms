//
//  CreatePatientViewController.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 17-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit

protocol CreatePatientViewControllerDelegate: class {
    func createPatientViewController(_ viewController: CreatePatientViewController, didCreatePatient patient: PatientEntity)
    func createPatientViewControllerDidCancel(_ viewController: CreatePatientViewController)
}

class CreatePatientViewController: BaseViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    // MARK: - Declarations
    
    @IBOutlet weak var incidentNameTextField: UITextField!
    @IBOutlet weak var triageTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var scrollViewButtomConstraint: NSLayoutConstraint!
    
    private var incidentNamePickerView: UIPickerView!
    private var triagePickerView: UIPickerView!
    
    
    weak var delegate: CreatePatientViewControllerDelegate?
    
    
    // MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Register Patient"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "navbar-logout"), style: .plain, target: self, action: #selector(logout))
        
        setupPickerViews()
        
        let tapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        startMonitoringKeyboardNotifications()
    }
    
    deinit {
        stopMonitoringKeyboardNotifications()
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        if let firstName = firstNameTextField.text,
            let lastName = lastNameTextField.text,
            let incidentName = incidentNameTextField.text,
            let triage = TriageCategory(rawValue: triagePickerView.selectedRow(inComponent: 0)) {
            
            let patient = PatientEntity(withIncidentName: incidentName,
                                        firstName: firstName,
                                        lastName: lastName,
                                        triageCategory: triage)
            delegate?.createPatientViewController(self, didCreatePatient: patient)
        } else {
            showAlert(with: nil, message: "Please enter all information")
        }
    }
    
    @objc func cancel() {
        delegate?.createPatientViewControllerDidCancel(self)
    }
    
    // MARK: - UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == incidentNamePickerView {
            return RepositoryFactory.repository().incidentList().count
        }
        
        return TriageCategory.allCases.count
    }
    
    // MARK: - UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        if pickerView == triagePickerView {
            let view = UIView()
            view.backgroundColor = TriageCategory(rawValue: row)?.color()
            return view
        }
        
        let label = UILabel()
        label.text = RepositoryFactory.repository().incidentList()[row].name
        label.textAlignment = .center
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == triagePickerView {
            triageTextField.backgroundColor = TriageCategory(rawValue: row)?.color()
            return
        }
        
        incidentNameTextField.text = RepositoryFactory.repository().incidentList()[row].name
    }
    
    // MARK: - helpers
    
    func setupPickerViews() {
        incidentNamePickerView = pickerView()
        incidentNameTextField.inputView = incidentNamePickerView
        if RepositoryFactory.repository().incidentList().count > 0 {
            incidentNameTextField.text = RepositoryFactory.repository().incidentList()[0].name
        }
        
        triagePickerView = pickerView()
        triageTextField.inputView = triagePickerView
        if TriageCategory.allCases.count > 0 {
            triageTextField.backgroundColor = TriageCategory(rawValue: 0)?.color()
        }
    }
    
    func pickerView() -> UIPickerView {
        
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        
        return pickerView
    }
    
    // MARK: - Keyboard handling
    
    func startMonitoringKeyboardNotifications() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIWindow.keyboardWillShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIWindow.keyboardWillHideNotification,
                                               object: nil)
    }
    
    func stopMonitoringKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self,
                                                  name: UIWindow.keyboardWillShowNotification,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: UIWindow.keyboardWillHideNotification,
                                                  object: nil)
        
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        
        var keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as AnyObject).cgRectValue.height
        
        if let tabBarController = tabBarController {
            keyboardHeight = keyboardHeight - tabBarController.tabBar.frame.size.height
        }
        
        UIView.animate(withDuration: 0.1) {
            self.scrollViewButtomConstraint.constant = keyboardHeight
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        
        UIView.animate(withDuration: 0.1) {
            self.scrollViewButtomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}
