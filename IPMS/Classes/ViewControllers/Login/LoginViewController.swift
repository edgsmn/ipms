//
//  LoginViewController.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 16-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit
import GoogleSignIn

class LoginViewController: BaseViewController, GIDSignInUIDelegate {

    // MARK: Properties
    
    @IBOutlet weak var signInButton: GIDSignInButton!
    
    // MARK: Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signInButton.style = .wide
        GIDSignIn.sharedInstance()?.uiDelegate = self
    }
}
