//
//  MainViewController.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 16-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit
import GoogleSignIn

class RootViewController: UIPageViewController, GIDSignInDelegate {
    
    // MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        GIDSignIn.sharedInstance()?.delegate = self
        showLogin()
    }
    
    // MARK: - Screen management
    
    private func showLogin() {
        let loginViewController = LoginViewController()
        setViewControllers([loginViewController], direction: .forward, animated: true, completion: nil)
    }
    
    private func showMain() {
        let viewController = UINavigationController.init(rootViewController: MainViewController())
        setViewControllers([viewController], direction: .forward, animated: true, completion: nil)
    }
    
    // MARK: - GIDSignInDelegate
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            showAlert(withMessage: error.localizedDescription)
        } else {
            showMain()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        showLogin()
    }
    
    // MARK: - helpers
    
    func showAlert(withMessage message: String) {
        
        let alertController = UIAlertController.init(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        
        present(alertController, animated: true, completion: nil)
    }
}
