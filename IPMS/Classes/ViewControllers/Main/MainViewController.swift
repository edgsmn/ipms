//
//  MainViewController.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 16-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit
import GoogleSignIn

class MainViewController: BaseViewController {
    
    // MARK: - Declarations
    
    @IBOutlet weak var greetingsLabel: UILabel!
    
    // MARK: - Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Main window"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "navbar-logout"), style: .plain, target: self, action: #selector(logout))
        
        if let name = GIDSignIn.sharedInstance()?.currentUser.profile.name {
            greetingsLabel.text = "Hello \(name)"
        }
    }
    
    // MARK: - UI actions
    
    @IBAction func incidentsButtonPressed(_ sender: Any) {
        let incidentsViewController = IncidentListViewController()
        navigationController?.pushViewController(incidentsViewController, animated: true)
    }
    
    @IBAction func patientsButtonPressed(_ sender: Any) {
        let patientsViewController = PatientListViewController()
        navigationController?.pushViewController(patientsViewController, animated: true)
    }
}
