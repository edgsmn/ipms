//
//  PatientListDataModel.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 17-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit

enum PatientSortingOption: String, CaseIterable {
    case nameASC = "Incident name ascending"
    case nameDESC = "Incident name descending"
}

protocol PatientListDataModelDelegate: class {
    func patientListDataModelDidUpdateData(_ dataModel: PatientListDataModel)
}

class PatientListDataModel {
    
    // MARK: - Declarations
    
    weak var delegate: PatientListDataModelDelegate?
    
    private var defaultList: [PatientEntity] = []
    public private(set) var listToDisplay: [PatientEntity] = []
    
    private var nameSortingOption: PatientSortingOption?
    private var triageFilter: TriageCategory?
    private var textFilter: String?
    
    // MARK: - Methods
    
    init() {
        loadPatients()
    }
    
    func sort(by option:PatientSortingOption) {
        
        nameSortingOption = option
        
        switch option {
        case .nameASC:
            listToDisplay = listToDisplay.sorted(by: { $0.incidentName < $1.incidentName })
            
        case .nameDESC:
            listToDisplay = listToDisplay.sorted(by: { $0.incidentName > $1.incidentName })
        }
        delegate?.patientListDataModelDidUpdateData(self)
    }
    
    
    func filterList(for searchString: String) {
        
        textFilter = searchString
        if searchString == "" {
            listToDisplay = defaultList
        } else {
            listToDisplay = defaultList.filter({ (patient: PatientEntity) -> Bool in
                return patient.incidentName.localizedCaseInsensitiveContains(searchString)
                }
            )
        }
        
        if let triageFilter = triageFilter {
            listToDisplay = listToDisplay.filter({ $0.triageCategory == triageFilter})
        }
        
        if let option = nameSortingOption {
            sort(by: option)
        }
        
        delegate?.patientListDataModelDidUpdateData(self)
    }
    
    func addTriageFilter(_ filter: TriageCategory?) {
        triageFilter = filter
        
        if let triageFilter = triageFilter {
            listToDisplay = defaultList.filter({ $0.triageCategory == triageFilter})
        } else {
            listToDisplay = defaultList
        }
        
        if let searchString = textFilter {
            if searchString != "" {
                listToDisplay = listToDisplay.filter({ (patient: PatientEntity) -> Bool in
                    return patient.incidentName.localizedCaseInsensitiveContains(searchString)
                    }
                )
            }
        }
        
        delegate?.patientListDataModelDidUpdateData(self)
    }
    
    func addPatient(_ patient: PatientEntity) {
        RepositoryFactory.repository().addPatient(patient)
        loadPatients()
    }
    
    private func loadPatients() {
        defaultList = RepositoryFactory.repository().patientList()
        listToDisplay = defaultList
        nameSortingOption = nil
        triageFilter = nil
        delegate?.patientListDataModelDidUpdateData(self)
    }
}
