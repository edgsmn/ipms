//
//  PatientsViewController.swift
//  IPMS
//
//  Created by Edgaras Simanavicius on 16-02-2019.
//  Copyright © 2019 Edgaras Simanavicius. All rights reserved.
//

import UIKit

class PatientListViewController: BaseListViewController, PatientListDataModelDelegate, CreatePatientViewControllerDelegate, TriageSelectorViewControllerDelegate {

    // MARK: - Declarations
    
    let dataModel = PatientListDataModel()
    
    // MARK: - Methods
    
    init() {
        super.init(nibName: String(describing: BaseListViewController.self), bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Patients"
        dataModel.delegate = self
        
        let addItem = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(addPatient))
        let logoutItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "navbar-logout"), style: .plain, target: self, action: #selector(logout))
        let sortOrFilterItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "navbar-sort-filter"), style: .plain, target: self, action: #selector(filterOrSort))
        
        navigationItem.rightBarButtonItems = [addItem, sortOrFilterItem, logoutItem]
    }
    
    // MARK: - BaseListViewController
    
    override func listToDisplay() -> [Any] {
        return dataModel.listToDisplay
    }
    
    override func customCell(for tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: "PatientsCell")
        
        if let patient = listToDisplay()[indexPath.row] as? PatientEntity {
            
            cell.textLabel?.text = patient.incidentName
            cell.detailTextLabel?.text = "\(patient.firstName) \(patient.lastName)"
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            view.backgroundColor = patient.triageCategory.color()
            cell.accessoryView = view
        }
        
        return cell
    }
    
    override func filterList(with text: String) {
        dataModel.filterList(for: text)
    }
    
    // MARK: - PatientListDataModelDelegate
    
    func patientListDataModelDidUpdateData(_ dataModel: PatientListDataModel) {
        tableView.reloadData()
    }
    
    // MARK: - CreatePatientViewControllerDelegate
    
    func createPatientViewController(_ viewController: CreatePatientViewController, didCreatePatient patient: PatientEntity) {
        dataModel.addPatient(patient)
        navigationController?.popViewController(animated: true)
    }
    
    func createPatientViewControllerDidCancel(_ viewController: CreatePatientViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - TriageSelectorViewControllerDelegate
    
    func triageSelectorViewController(_ viewController: TriageSelectorViewController, didSelectTriage triage: TriageCategory?) {
        dataModel.addTriageFilter(triage)
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UI actions
    
    @objc func addPatient() {
        let createPatientViewController = CreatePatientViewController()
        createPatientViewController.delegate = self
        navigationController?.pushViewController(createPatientViewController, animated: true)
    }
    
    @objc func filterOrSort() {
        let alertController = UIAlertController.init(title: "Select action", message: nil, preferredStyle: .alert)
        
        let sortAction = UIAlertAction.init(title: "Sort", style: .default) { (action) in
            self.sort()
        }
        
        let filterAction = UIAlertAction.init(title: "Filter triage", style: .default) { (action) in
            self.filter()
        }
        
        alertController.addAction(sortAction)
        alertController.addAction(filterAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func filter() {
        let viewController = TriageSelectorViewController()
        viewController.delegate = self
        viewController.modalPresentationStyle = .overCurrentContext
        viewController.modalTransitionStyle = .crossDissolve
        present(viewController, animated: true, completion: nil)
    }
    
    func sort() {
        
        let alertController = UIAlertController.init(title: "Sort by", message: nil, preferredStyle: .alert)
        
        PatientSortingOption.allCases.forEach {
            let option: PatientSortingOption = $0
            let action = UIAlertAction.init(title: option.rawValue, style: .default, handler: { (action) in
                self.dataModel.sort(by: option)
            })
            alertController.addAction(action)
        }
        
        present(alertController, animated: true, completion: nil)
    }
}
